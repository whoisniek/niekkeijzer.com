+++
title = "Burrow"
date = 2018-12-28T13:39:42+01:00
+++

I _love_ domotics and automation, what is a better to combine these two
 than to automate home automation?

In this project I am working on creating an easy way to get started with
 home automation. This is done by providing a few building blocks in the
 form of a set of [Ansible](https://www.ansible.com) playbooks to
 configure one or more hosts running [Docker](http://docker.com). To
 manage the Swarm and to allow users to add features _they_ need,
 [Portainer](https://www.portainer.io) is configured with a set of
 predefined apps the user can choose from.
