+++
title = "Personal site"
date = 2018-12-28T13:39:42+01:00
+++

Almost everyone benefits from having an online presence. However, I feel
 that most current sites feel bloated, especially for a backend
 developer. That is why I created this site using a static site generator
 called [Hugo](https://www.gohugo.io)

The goal of this project is to first and foremost to give those
 interested some insight into what I do. Secondly I wanted a _fast_ site
 where content has the highest priority.
