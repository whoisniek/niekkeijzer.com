+++
title = "Projects"
date = "2018-12-28T11:13:17+01:00"
menu = "main"
+++

Some of the projects I have worked on can be found here. Note that
 this only a selection and my open source projects can be found on my
 _Github_ and _Gitlab_ pages listed below.
