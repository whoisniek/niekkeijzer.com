const glob = require('glob-all');
const gulp = require('gulp');
const htmlmin = require('gulp-htmlmin');
const critical = require('critical').stream;
const spawn = require('child_process').spawn;

const suppressHugoErrors = false;

gulp.task('critical', function () {
    return gulp.src('public/**/*.html')
        .pipe(critical({
          base: 'public/',
          inline: true,
          minify: true,
          extract: true,
          inline: true,
          dimensions: [
            {
              width: 320,
              height: 480
            },
            {
              width: 375,
              height: 565
            },
            {
              width: 900,
              height: 1200
            }
          ],
          css: glob.sync(['public/**/*.css'])
        }))
        .on('error', function(err) { log.error(err.message); })
        .pipe(gulp.dest('public'));
});

gulp.task('minify', () => {
  return gulp.src('public/**/*.html')
    .pipe(htmlmin({ collapseWhitespace: true }))
    .pipe(gulp.dest('public'));
});

gulp.task('hugo', (done) => {
  let args = [];
  if (process.env.BUILD_DRAFTS) {
    args.push(...['--buildDrafts', '--buildFuture']);
  }

  if (process.env.DEPLOY_PRIME_URL) {
    args.push('--baseURL=' + process.env.DEPLOY_PRIME_URL);
  } else if (process.env.URL) {
    args.push('--baseURL=' + process.env.URL);
  }

  console.log(args);

  return spawn('hugo', args, {stdio: 'inherit'}).on('close', (code) => {
    if (code === 0) {
      done();
    } else {
      done('Hugo build command failed');
    }
  });
});

gulp.task('build', gulp.series('hugo', 'critical', 'minify'));
gulp.task('default', gulp.series('build'));
